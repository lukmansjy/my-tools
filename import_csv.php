<?php
ini_set('max_execution_time', 0);

$csv_file = 'file_name.csv';
$table_target = 'table_name';

$host       = 'localhost';
$user       = 'root';
$password   = '';
$database   = 'YOUR DB';

$conn = mysqli_connect($host, $user, $password, $database);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Get field table
$query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_SCHEMA = '$database' AND TABLE_NAME = '$table_target'";
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));

$field_tbl = [];
while($row = mysqli_fetch_assoc($result)){
    $field_tbl[] = $row['COLUMN_NAME'];
    
}

// Read csv
$column_csv = [];
$row = 1;
$count_insert = 0;
if (($handle = fopen($csv_file, 'r')) !== FALSE) {
    while (($data_csv = fgetcsv($handle, ',')) !== FALSE) {
        $num = count($data_csv);
        if($row == 1){
            // Check colom csv sesuai sama yg dibutuhin di table atau tidak
            foreach($data_csv as $dt){
                $column_csv[] = $dt;
            }
            foreach($field_tbl as $f_tbl){
                $key_csv = array_search($f_tbl, $column_csv);
                
                if(!is_numeric($key_csv)){
                    echo "____________ $f_tbl not found in csv file ____________";
                    fclose($handle);
                    mysqli_close($conn);
                    die();
                }
                $key_csv_to_db[$f_tbl] = $key_csv;
            }
        }else{
            // Mengurutkan data csv sesuai sama field table
            $value = [];
            foreach($field_tbl as $f_tbl){
                $index_data_in_csv = $key_csv_to_db[$f_tbl];
                $data_escape = mysqli_real_escape_string($conn, $data_csv[$index_data_in_csv]);
                $value[] = "'$data_escape'";
            }
            $insert_key = implode(',', $field_tbl);
            $insert_value = implode(',', $value);

            // Insert to db
            $sql = "INSERT INTO $table_target ($insert_key) VALUES ($insert_value)";
            $result_insert = mysqli_query($conn, $sql) or die(mysqli_error($conn));
            if($result_insert){
                $count_insert++;
                echo "Success insert in $count_insert row\n";
            }else{
                echo "Error in counter $count_insert \n";
                echo "sql query:\n$sql";
                fclose($handle);
                mysqli_close($conn);
                die();
            }
        }
        $row++;
    }

    fclose($handle);
    mysqli_close($conn);
}
